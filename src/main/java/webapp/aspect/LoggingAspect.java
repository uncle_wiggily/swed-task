package webapp.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class LoggingAspect {

    private static Logger logger = Logger.getLogger(LoggingAspect.class.getName());

    @Before("execution(* webapp.controller.*.*(..))")
    public void logControllerEvent(JoinPoint joinPoint) {
        logger.info("Endpoint call: " + joinPoint.toShortString());
    }

}
