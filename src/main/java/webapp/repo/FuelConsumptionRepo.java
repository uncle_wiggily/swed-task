package webapp.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import webapp.entity.FuelConsumptionEntity;
import webapp.report.FuelConsumptionsByFuelTypeAndMonthReport;
import webapp.report.MoneySpentByMonthReport;

import java.util.List;

@Repository
public interface FuelConsumptionRepo extends CrudRepository<FuelConsumptionEntity, Long> {

    @Query(value =
            "SELECT YEAR(date_of_consumption) as year, MONTH(date_of_consumption) as month,\n" +
            "   ROUND(SUM(volume_in_liters * price_per_liter), 3) as totalPrice \n" +
            "   FROM fuel_consumption_entity\n" +
            "GROUP BY year, month\n" +
            "ORDER BY year, month", nativeQuery = true)
    List<MoneySpentByMonthReport> getMoneySpentByMonth();

    @Query(value =
            "SELECT YEAR(date_of_consumption) as year, MONTH(date_of_consumption) as month,\n" +
            "   ROUND(SUM(volume_in_liters * price_per_liter), 3) as totalPrice \n" +
            "   FROM fuel_consumption_entity\n" +
            "   WHERE driver_id = :driverId\n" +
            "GROUP BY year, month\n" +
            "ORDER BY year, month", nativeQuery = true)
    List<MoneySpentByMonthReport> getMoneySpentByMonth(@Param("driverId") long driverId);

    @Query(value =
            "SELECT id, fuel_type, price_per_liter, volume_in_liters,\n" +
            "    date_of_consumption, driver_id\n" +
            "    FROM FUEL_CONSUMPTION_ENTITY\n" +
            "WHERE MONTH(date_of_consumption) = :monthNumber AND YEAR(date_of_consumption) = YEAR(NOW())\n" +
            "ORDER BY date_of_consumption", nativeQuery = true)
    List<FuelConsumptionEntity> getFuelConsumptionsForSpecificMonth(@Param("monthNumber") int monthNumber);

    @Query(value =
            "SELECT id, fuel_type, price_per_liter, volume_in_liters,\n" +
            "    date_of_consumption, driver_id\n" +
            "    FROM FUEL_CONSUMPTION_ENTITY\n" +
            "WHERE MONTH(date_of_consumption) = :monthNumber AND YEAR(date_of_consumption) = YEAR(NOW()) AND driver_id = :driverId\n" +
            "ORDER BY date_of_consumption", nativeQuery = true)
    List<FuelConsumptionEntity> getFuelConsumptionsForSpecificMonth(@Param("monthNumber") int monthNumber, @Param("driverId") long driverId);

    @Query(value =
            "SELECT YEAR(date_of_consumption) as year, MONTH(date_of_consumption) as month,\n" +
            "    fuel_type as fuelType, SUM(volume_in_liters) as totalVolume,\n" +
            "    AVG(price_per_liter) as averagePrice, ROUND(SUM(volume_in_liters * price_per_liter), 3) as totalPrice\n" +
            "    FROM fuel_consumption_entity\n" +
            "GROUP BY fuelType, year, month\n" +
            "ORDER BY year, month", nativeQuery = true)
    List<FuelConsumptionsByFuelTypeAndMonthReport> getFuelConsumptionsGroupedByFuelTypeAndMonth();

    @Query(value =
            "SELECT YEAR(date_of_consumption) as year, MONTH(date_of_consumption) as month,\n" +
            "    fuel_type as fuelType, SUM(volume_in_liters) as totalVolume,\n" +
            "    AVG(price_per_liter) as averagePrice, ROUND(SUM(volume_in_liters * price_per_liter), 3) as totalPrice\n" +
            "    FROM fuel_consumption_entity\n" +
            "    WHERE driver_id = :driverId\n" +
            "GROUP BY fuelType, year, month\n" +
            "ORDER BY year, month", nativeQuery = true)
    List<FuelConsumptionsByFuelTypeAndMonthReport> getFuelConsumptionsGroupedByFuelTypeAndMonth(@Param("driverId") long driverId);

}
