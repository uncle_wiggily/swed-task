package webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import webapp.entity.FuelConsumptionEntity;
import webapp.report.FuelConsumptionsByFuelTypeAndMonthReport;
import webapp.report.MoneySpentByMonthReport;
import webapp.service.FuelConsumptionReportService;

import java.util.List;

@RestController
@RequestMapping(value = "/fuel_consumptions")
public class FuelConsumptionReportController {

    private final FuelConsumptionReportService fuelConsumptionReportService;

    @Autowired
    public FuelConsumptionReportController(FuelConsumptionReportService fuelConsumptionReportService) {
        this.fuelConsumptionReportService = fuelConsumptionReportService;
    }

    @GetMapping("/money_spent_by_month")
    public List<MoneySpentByMonthReport> getMoneySpentByMonth() {
        return this.fuelConsumptionReportService.getMoneySpentByMonth();
    }

    @GetMapping("/money_spent_by_month/{driverId}")
    public List<MoneySpentByMonthReport> getMoneySpentByMonth(@PathVariable long driverId) {
        return this.fuelConsumptionReportService.getMoneySpentByMonth(driverId);
    }

    @GetMapping("/for_specific_month")
    public List<FuelConsumptionEntity> getFuelConsumptionsForSpecificMonth(@RequestParam int monthNumber) {
        return this.fuelConsumptionReportService.getFuelConsumptionsForSpecificMonth(monthNumber);
    }

    @GetMapping("/for_specific_month/{driverId}")
    public List<FuelConsumptionEntity> getFuelConsumptionsForSpecificMonth(@RequestParam int monthNumber, @PathVariable long driverId) {
        return this.fuelConsumptionReportService.getFuelConsumptionsForSpecificMonth(monthNumber, driverId);
    }

    @GetMapping("/grouped_by_fuel_type_and_month")
    public List<FuelConsumptionsByFuelTypeAndMonthReport> getFuelConsumptionsGroupedByFuelTypeAndMonth() {
        return this.fuelConsumptionReportService.getFuelConsumptionsGroupedByFuelTypeAndMonth();
    }

    @GetMapping("/grouped_by_fuel_type_and_month/{driverId}")
    public List<FuelConsumptionsByFuelTypeAndMonthReport> getFuelConsumptionsGroupedByFuelTypeAndMonth(@PathVariable long driverId) {
        return this.fuelConsumptionReportService.getFuelConsumptionsGroupedByFuelTypeAndMonth(driverId);
    }

}
