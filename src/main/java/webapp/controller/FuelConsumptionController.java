package webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import webapp.dto.FuelConsumptionDto;
import webapp.dto.MultipleFuelConsumptionsDto;
import webapp.entity.FuelConsumptionEntity;
import webapp.service.FuelConsumptionService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/fuel_consumptions")
public class FuelConsumptionController {

    private final FuelConsumptionService fuelConsumptionService;

    @Autowired
    public FuelConsumptionController(FuelConsumptionService fuelConsumptionService) {
        this.fuelConsumptionService = fuelConsumptionService;
    }

    @PostMapping
    public ResponseEntity registerConsumption(@Valid @RequestBody FuelConsumptionDto fuelConsumptionDto) {
        FuelConsumptionEntity fuelConsumption = fuelConsumptionService.saveConsumption(fuelConsumptionDto.getEntity());
        return ResponseEntity.status(HttpStatus.OK).body(fuelConsumption);
    }

    @PostMapping("/multiple")
    public ResponseEntity registerMultipleConsumptions(@Valid @RequestBody MultipleFuelConsumptionsDto multipleFuelConsumptionsDto) {
        List<FuelConsumptionEntity> fuelConsumptionEntityList = multipleFuelConsumptionsDto.getEntity();
        this.fuelConsumptionService.saveMultipleConsumptions(fuelConsumptionEntityList);
        return new ResponseEntity(HttpStatus.OK);
    }

}
