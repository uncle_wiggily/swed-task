package webapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webapp.entity.FuelConsumptionEntity;
import webapp.repo.FuelConsumptionRepo;

import java.util.List;

@Service
public class FuelConsumptionService {

   private final FuelConsumptionRepo fuelConsumptionRepo;

    @Autowired
    public FuelConsumptionService(FuelConsumptionRepo fuelConsumptionRepo) {
        this.fuelConsumptionRepo = fuelConsumptionRepo;
    }

    public FuelConsumptionEntity saveConsumption(FuelConsumptionEntity fuelConsumption) {
        return this.fuelConsumptionRepo.save(fuelConsumption);
    }

    public void saveMultipleConsumptions(List<FuelConsumptionEntity> fuelConsumptionEntityList) {
        this.fuelConsumptionRepo.saveAll(fuelConsumptionEntityList);
    }

}
