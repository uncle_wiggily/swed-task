package webapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webapp.entity.FuelConsumptionEntity;
import webapp.report.FuelConsumptionsByFuelTypeAndMonthReport;
import webapp.report.MoneySpentByMonthReport;
import webapp.repo.FuelConsumptionRepo;

import java.util.List;

@Service
public class FuelConsumptionReportService {

    private final FuelConsumptionRepo fuelConsumptionRepo;

    @Autowired
    public FuelConsumptionReportService(FuelConsumptionRepo fuelConsumptionRepo) {
        this.fuelConsumptionRepo = fuelConsumptionRepo;
    }

    public List<MoneySpentByMonthReport> getMoneySpentByMonth() {
        return this.fuelConsumptionRepo.getMoneySpentByMonth();
    }

    public List<MoneySpentByMonthReport> getMoneySpentByMonth(long driverId) {
        return this.fuelConsumptionRepo.getMoneySpentByMonth(driverId);
    }

    public List<FuelConsumptionEntity> getFuelConsumptionsForSpecificMonth(int monthNumber) {
        return this.fuelConsumptionRepo.getFuelConsumptionsForSpecificMonth(monthNumber);
    }

    public List<FuelConsumptionEntity> getFuelConsumptionsForSpecificMonth(int monthNumber, long driverId) {
        return this.fuelConsumptionRepo.getFuelConsumptionsForSpecificMonth(monthNumber, driverId);
    }

    public List<FuelConsumptionsByFuelTypeAndMonthReport> getFuelConsumptionsGroupedByFuelTypeAndMonth() {
        return this.fuelConsumptionRepo.getFuelConsumptionsGroupedByFuelTypeAndMonth();
    }

    public List<FuelConsumptionsByFuelTypeAndMonthReport> getFuelConsumptionsGroupedByFuelTypeAndMonth(long driverId) {
        return this.fuelConsumptionRepo.getFuelConsumptionsGroupedByFuelTypeAndMonth(driverId);
    }

}
