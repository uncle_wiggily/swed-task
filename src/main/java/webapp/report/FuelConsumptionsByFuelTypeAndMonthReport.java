package webapp.report;

import webapp.entity.FuelType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public interface FuelConsumptionsByFuelTypeAndMonthReport {

    int getYear();
    int getMonth();
    @Enumerated(EnumType.STRING)
    FuelType getFuelType();
    double getTotalVolume();
    double getAveragePrice();
    double getTotalPrice();

}
