package webapp.report;

public interface MoneySpentByMonthReport {

    int getYear();
    int getMonth();
    double getTotalPrice();

}
