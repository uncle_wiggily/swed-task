package webapp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public enum FuelType {

    @JsonProperty("D") DIESEL("D"),
    @JsonProperty("98") GASOLINE98("98"),
    @JsonProperty("95") GASOLINE95("95");

    private final String label;

    FuelType(String label) {
        this.label = label;
    }

}
