package webapp.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@Data
public class FuelConsumptionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    private FuelType fuelType;

    private double pricePerLiter;

    private double volumeInLiters;

    @Transient
    private BigDecimal totalPrice;

    private LocalDate dateOfConsumption;

    private int driverId;

    @PostLoad@PostPersist
    private void calculateTotalPrice() {
        double totalPrice = this.pricePerLiter * this.volumeInLiters;
        this.totalPrice = new BigDecimal(totalPrice).setScale(3, RoundingMode.HALF_UP);
    }

    public FuelConsumptionEntity(FuelType fuelType, double pricePerLiter, double volumeInLiters,
                                 LocalDate dateOfConsumption, int driverId) {
        this.fuelType = fuelType;
        this.pricePerLiter = pricePerLiter;
        this.volumeInLiters = volumeInLiters;
        this.dateOfConsumption = dateOfConsumption;
        this.driverId = driverId;
    }

}
