package webapp.dto;

public interface BaseDto<T> {

    T getEntity();

}
