package webapp.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import webapp.entity.FuelConsumptionEntity;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Component
@Getter@Setter
public class MultipleFuelConsumptionsDto implements BaseDto<List<FuelConsumptionEntity>> {

    @NotEmpty
    @Valid
    private List<FuelConsumptionDto> fuelConsumptionDtoList;

    @Override
    public List<FuelConsumptionEntity> getEntity() {
        List<FuelConsumptionEntity> fuelConsumptionEntityList = new ArrayList<>();
        this.fuelConsumptionDtoList.forEach( dto ->
                fuelConsumptionEntityList.add(dto.getEntity()));
        return fuelConsumptionEntityList;
    }

}
