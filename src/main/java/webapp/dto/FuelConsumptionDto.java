package webapp.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import webapp.entity.FuelConsumptionEntity;
import webapp.entity.FuelType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Component
@Getter@Setter
public class FuelConsumptionDto implements BaseDto<FuelConsumptionEntity> {

    private FuelType fuelType;

    @NotNull
    @Min(value = 0)
    private Double pricePerLiter;

    @NotNull
    @Min(value = 0)
    private Double volumeInLiters;

    @NotNull
    @DateTimeFormat
    private LocalDate dateOfConsumption;

    @NotNull
    @Min(value = 0)
    private Integer driverId;

    @Override
    public FuelConsumptionEntity getEntity() {
        return new FuelConsumptionEntity(fuelType, pricePerLiter, volumeInLiters, dateOfConsumption, driverId);
    }

}
