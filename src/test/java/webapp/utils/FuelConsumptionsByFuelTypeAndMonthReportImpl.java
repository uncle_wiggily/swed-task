package webapp.utils;

import lombok.*;
import webapp.entity.FuelType;
import webapp.report.FuelConsumptionsByFuelTypeAndMonthReport;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FuelConsumptionsByFuelTypeAndMonthReportImpl implements FuelConsumptionsByFuelTypeAndMonthReport {

    private int year;
    private int month;
    private FuelType fuelType;
    private double totalVolume;
    private double averagePrice;
    private double totalPrice;

}
