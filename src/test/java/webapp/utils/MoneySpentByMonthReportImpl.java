package webapp.utils;

import lombok.*;
import webapp.report.MoneySpentByMonthReport;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MoneySpentByMonthReportImpl implements MoneySpentByMonthReport {

    private int year;
    private int month;
    private double totalPrice;

}
