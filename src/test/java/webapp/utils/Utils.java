package webapp.utils;

import webapp.entity.FuelConsumptionEntity;
import webapp.entity.FuelType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    public static List<FuelConsumptionEntity> generateTestEntitiesWithAllFuelTypes(LocalDate dateOfConsumption) {
        int counter = 1;
        List<FuelConsumptionEntity> testFuelConsumptionEntities = new ArrayList<>();
        List<FuelType> allFuelTypes = Arrays.asList(FuelType.values());
        for (FuelType fuelType: allFuelTypes) {
            FuelConsumptionEntity fuelConsumptionEntity =
                    new FuelConsumptionEntity(fuelType, counter * 10, counter * 20,
                            dateOfConsumption, (int) counter * 100);
            fuelConsumptionEntity.setTotalPrice(
                    new BigDecimal(Math.pow(counter, 2) * 200).setScale(3, RoundingMode.HALF_UP));
            testFuelConsumptionEntities.add(fuelConsumptionEntity);
            counter++;
        }

        return testFuelConsumptionEntities;
    }

}
