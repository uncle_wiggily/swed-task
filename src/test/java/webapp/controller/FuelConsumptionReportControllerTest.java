package webapp.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import webapp.entity.FuelConsumptionEntity;
import webapp.entity.FuelType;
import webapp.report.FuelConsumptionsByFuelTypeAndMonthReport;
import webapp.report.MoneySpentByMonthReport;
import webapp.service.FuelConsumptionService;
import webapp.utils.FuelConsumptionsByFuelTypeAndMonthReportImpl;
import webapp.utils.MoneySpentByMonthReportImpl;
import webapp.utils.Utils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class FuelConsumptionReportControllerTest {

    @Autowired private MockMvc mvc;
    @Autowired private FuelConsumptionService fuelConsumptionService;
    @Autowired private ObjectMapper mapper;

    @Test
    public void moneySpentByMonthReportTest() throws Exception {
        addTestFuelConsumptionToDatabase();
        List<MoneySpentByMonthReport> moneySpentByMonthReportList = getTestReports();

        MvcResult requestResult = mvc.perform(MockMvcRequestBuilders
            .get("/fuel_consumptions/money_spent_by_month"))
            .andExpect(status().isOk())
            .andReturn();

        assertEquals(moneySpentByMonthReportList,
                mapper.readValue(requestResult.getResponse().getContentAsString(),
                        new TypeReference<List<MoneySpentByMonthReportImpl>>(){}));
    }

    public void addTestFuelConsumptionToDatabase() {
        List<FuelConsumptionEntity> fuelConsumptionEntityList =
                Utils.generateTestEntitiesWithAllFuelTypes(LocalDate.now());
        fuelConsumptionEntityList.addAll(
                Utils.generateTestEntitiesWithAllFuelTypes(LocalDate.now().minusYears(1)));
        this.fuelConsumptionService.saveMultipleConsumptions(fuelConsumptionEntityList);
    }

    private List<MoneySpentByMonthReport> getTestReports() {
        LocalDate now = LocalDate.now();
        MoneySpentByMonthReport moneySpentByMonthReportThisYear =
                new MoneySpentByMonthReportImpl(
                        now.getYear(), now.getMonthValue(), 2800);
        LocalDate yearAgo = now.minusYears(1);
        MoneySpentByMonthReport moneySpentByMonthReportPrevYear =
                new MoneySpentByMonthReportImpl(
                        yearAgo.getYear(), yearAgo.getMonthValue(), 2800);
        return Arrays.asList(moneySpentByMonthReportPrevYear, moneySpentByMonthReportThisYear);
    }

    @Test
    public void consumptionsByFuelAndMonthReportTest() throws Exception {
        this.fuelConsumptionService.saveConsumption(
                new FuelConsumptionEntity(FuelType.DIESEL, 2, 20, LocalDate.now(), 500));
        this.fuelConsumptionService.saveConsumption(
                new FuelConsumptionEntity(FuelType.DIESEL, 4, 100, LocalDate.now(), 500));
        LocalDate now = LocalDate.now();

        MvcResult requestResult = mvc.perform(MockMvcRequestBuilders
            .get("/fuel_consumptions/grouped_by_fuel_type_and_month"))
            .andExpect(status().isOk())
            .andReturn();
        String resultAsJsonString = requestResult.getResponse().getContentAsString();
        List<FuelConsumptionsByFuelTypeAndMonthReport> actualFuelConsumptionsByFuelTypeAndMonthReportList =
                mapper.readValue(resultAsJsonString,
                        new TypeReference<List<FuelConsumptionsByFuelTypeAndMonthReportImpl>>(){});
        assertEquals(new FuelConsumptionsByFuelTypeAndMonthReportImpl(
                now.getYear(), now.getMonthValue(), FuelType.DIESEL, 120, 3, 440),
                actualFuelConsumptionsByFuelTypeAndMonthReportList.get(0));
    }

    @Test
    public void consumptionForSpecifiedMonthTest() throws Exception {
        LocalDate now = LocalDate.now();

        List<FuelConsumptionEntity> fuelConsumptionEntityList =
                Utils.generateTestEntitiesWithAllFuelTypes(now);
        this.fuelConsumptionService.saveMultipleConsumptions(fuelConsumptionEntityList);

        MvcResult requestResult = mvc.perform(MockMvcRequestBuilders
                .get("/fuel_consumptions/for_specific_month?monthNumber=" + now.getMonthValue()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(fuelConsumptionEntityList,
                mapper.readValue(requestResult.getResponse().getContentAsString(),
                        new TypeReference<List<FuelConsumptionEntity>>(){}));
    }

    @Test
    public void getConsumptionsForMonthWithoutRecordsTests() throws Exception {
        MvcResult requestResult = mvc.perform(MockMvcRequestBuilders
                .get("/fuel_consumptions/for_specific_month?monthNumber="
                        + LocalDate.now().minusMonths(1).getMonthValue()))
                .andExpect(status().isOk())
                .andReturn();
        assertEquals("[]", requestResult.getResponse().getContentAsString());
    }

}
