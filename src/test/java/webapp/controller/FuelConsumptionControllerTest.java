package webapp.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import webapp.entity.FuelConsumptionEntity;
import webapp.entity.FuelType;
import webapp.utils.Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class FuelConsumptionControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper mapper;

    @Test
    public void saveFuelConsumptionTest() throws Exception {
        FuelConsumptionEntity fuelConsumptionEntity =
                new FuelConsumptionEntity(FuelType.DIESEL, 1.3, 20, LocalDate.now(), 10);
        fuelConsumptionEntity.setId(1L);
        fuelConsumptionEntity.setTotalPrice(new BigDecimal(1.3 * 20).setScale(3, RoundingMode.HALF_UP));

        mvc.perform(MockMvcRequestBuilders
                .post("/fuel_consumptions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(fuelConsumptionEntity)))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(mapper.writeValueAsString(fuelConsumptionEntity))));
    }

    @Test
    public void saveMultipleFuelConsumptionsTest() throws Exception {
        List<FuelConsumptionEntity> testFuelConsumptionEntities =
                Utils.generateTestEntitiesWithAllFuelTypes(LocalDate.now());

        mvc.perform(MockMvcRequestBuilders
                .post("/fuel_consumptions/multiple")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{" +
                            "\"fuelConsumptionDtoList\":" +
                            mapper.writeValueAsString(testFuelConsumptionEntities) +
                        "}"))
                .andExpect(status().isOk());
    }

    @Test
    public void saveConsumptionWithWrongDataTest() throws Exception {
        FuelConsumptionEntity fuelConsumptionEntity =
                new FuelConsumptionEntity(FuelType.DIESEL, 1.3, -20, LocalDate.now(), 10);
        fuelConsumptionEntity.setId(1L);
        fuelConsumptionEntity.setTotalPrice(new BigDecimal(1.3 * 20).setScale(3, RoundingMode.HALF_UP));

        mvc.perform(MockMvcRequestBuilders
                .post("/fuel_consumptions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(fuelConsumptionEntity)))
                .andExpect(status().isBadRequest());
    }

}
